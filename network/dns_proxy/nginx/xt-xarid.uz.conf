upstream xt_xarid {
  server 10.60.10.155:2000;
  keepalive 32;
}


server {
  listen 443 ssl http2;
  server_name xt-xarid.uz default_server;

  keepalive_timeout 120;

  gzip on;
  gzip_comp_level 5;
  gzip_types *;

  ssl_certificate /path/fullchain.pem;
  ssl_certificate_key /path/privkey.pem;

  ssl_dhparam /etc/nginx/ssl/dhparam.pem;
  ssl_session_timeout 24h;
  ssl_session_cache shared:SSL:2m;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers kEECDH+AES128:kEECDH:kEDH:-3DES:kRSA+AES128:kEDH+3DES:DES-CBC3-SHA:!RC4:!aNULL:!eNULL:!MD5:!EXPORT:!LOW:!SEED:!CAMELLIA:!IDEA:!PSK:!SRP:!SSLv2;
  ssl_prefer_server_ciphers on;
  add_header Strict-Transport-Security "max-age=31536000;";

  ssl_stapling on;
  ssl_stapling_verify on;

  include acme;

  location /front/static-pages/ {
    alias /front/static-pages/;
  }

  root /front/active;
  index index.html;

  location ~* \.(?:ico|css|js|gif|jpe?g|png)$ {
    expires max;
    add_header Pragma public;
    add_header Cache-Control "public, must-revalidate, proxy-revalidate";
  }

  location / {
    try_files $uri /index.html;

    add_header Last-Modified $date_gmt;
    add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0, s-maxage=0';
    add_header Pragma 'no-cache';
    expires off;
  }
}

server {
  listen 443 ssl http2;
  server_name api.xt-xarid.uz;

  keepalive_timeout 120;

  gzip on;
  gzip_comp_level 5;
  gzip_types *;

  ssl_certificate /path/fullchain.pem;
  ssl_certificate_key /path/privkey.pem;

  ssl_dhparam /etc/nginx/ssl/dhparam.pem;
  ssl_session_timeout 24h;
  ssl_session_cache shared:SSL:2m;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers kEECDH+AES128:kEECDH:kEDH:-3DES:kRSA+AES128:kEDH+3DES:DES-CBC3-SHA:!RC4:!aNULL:!eNULL:!MD5:!EXPORT:!LOW:!SEED:!CAMELLIA:!IDEA:!PSK:!SRP:!SSLv2;
  ssl_prefer_server_ciphers on;
  add_header Strict-Transport-Security "max-age=31536000;";

  ssl_stapling on;
  ssl_stapling_verify on;

  include acme;

  # prevents 502 Bad Gateway error
  large_client_header_buffers 8 32k;

  proxy_connect_timeout 600;
  proxy_send_timeout 600;
  proxy_read_timeout 600;
  send_timeout 600;

  location / {
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_set_header Host $host:$server_port;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Ssl on;
    proxy_set_header X-Forwarded-Proto https;

    # prevents 502 bad gateway error
    proxy_buffers 8 32k;
    proxy_buffer_size 64k;

    proxy_pass http://xt_xarid/;

    add_header Access-Control-Expose-Headers 'Date';
  }
}